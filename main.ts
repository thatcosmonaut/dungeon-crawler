declare global { let PROF_CAPTURE: boolean; }
PROF_CAPTURE = false; // set this to true to enable profiling

import * as jprof from "encompass-jprof";
import { Game } from "game/game";

let game: Game;

love.load = () => {
    love.graphics.setDefaultFilter("nearest")
    love.window.setMode(love.graphics.getWidth(), love.graphics.getHeight(), { vsync: false, msaa: 2 });
    love.math.setRandomSeed(os.time());
    love.mouse.setVisible(false);

    game = new Game();
    game.load();
};

love.update = (dt) => {
    game.update(dt);
};

love.draw = () => {
    game.draw();

    love.graphics.setBlendMode("alpha");
    love.graphics.setColor(1, 1, 1, 1);
    love.graphics.print("Current FPS: " + tostring(love.timer.getFPS()), 10, 10);
};

love.quit = () => {
    jprof.write("prof.mpack");
    return false;
};
