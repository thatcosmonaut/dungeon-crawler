import { Message } from "encompass-ecs";

export class SpawnPlayerMessage extends Message {
    public x: number;
    public y: number;
}
