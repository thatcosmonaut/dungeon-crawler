import { Message } from "encompass-ecs";

export class CreateTileMessage extends Message {
    public x: number;
    public y: number;
    public walkable: boolean;
}