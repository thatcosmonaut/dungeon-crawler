import { Engine, Reads, Message, Type } from "encompass-ecs";
import { CreateTileMessage } from "game/messages/create_tile";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";
import { TileComponent } from "game/components/tile";
import { TransformComponent } from "game/components/transform";
import { Vector } from "game/helpers/vector";

@Reads(CreateTileMessage)
export class CreateTileEngine extends Engine {

    private wall_texture: ImageData;

    public update() {
        const create_tile_messages = this.read_messages(CreateTileMessage);
        if (this.wall_texture == null) {
            this.wall_texture = love.image.newImageData('game/assets/textures/wall.png');
        }

        for (const create_tile_message of create_tile_messages.iterable()) {
            const tile_entity = this.create_entity();
            const tile_component = tile_entity.add_component(TileComponent);
            const transform = tile_entity.add_component(TransformComponent);
            transform.position = new Vector(create_tile_message.x, create_tile_message.y);
            tile_component.walkable = create_tile_message.walkable;
            tile_component.texture = this.wall_texture;
        }
    }
}