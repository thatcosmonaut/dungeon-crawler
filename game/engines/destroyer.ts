import { Detecter, Entity, Reads, Message, Type, Emits } from "encompass-ecs";
import { TileComponent } from "game/components/tile";
import { DestroyLevelMessage } from "game/messages/destroy_level";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";
import { GenerateLevelMessage } from "game/messages/generate_level";

@Reads(DestroyLevelMessage)
export class DestroyLevelDetecter extends Detecter {
    public component_types = [TileComponent];

    protected detect(entity: Entity) {
        const destroy_messages = this.read_messages(DestroyLevelMessage);
        for (const destroy_level_message of destroy_messages.iterable()) {
            entity.destroy();
        }
    }
}
