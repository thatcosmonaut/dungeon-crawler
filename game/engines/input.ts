import { Engine, Emits, Type, Message } from "encompass-ecs";
import { DestroyLevelMessage } from "game/messages/destroy_level";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";
import { GenerateLevelMessage } from "game/messages/generate_level";
import { CameraComponent } from "game/components/camera";
import { ChangeTransformMessage } from "game/messages/change_transform";
import { TransformComponent } from "game/components/transform";
import { Vector } from "game/helpers/vector";

@Emits(ChangeTransformMessage)
export class InputEngine extends Engine {

    public update() {
        const cameras = this.read_components(CameraComponent);
        for (const camera of cameras.iterable()) {
            const camera_entity = this.get_entity(camera.entity_id);
            if (camera_entity != null) {
                const transform = camera_entity.get_component(TransformComponent);
                const rot_speed = -love.timer.getDelta() * 0.5;

                const new_dir_x = transform.direction.x * Math.cos(-rot_speed) - transform.direction.y * Math.sin(-rot_speed);
                const new_dir_y = transform.direction.x * Math.sin(-rot_speed) + transform.direction.y * Math.cos(-rot_speed);
                const new_plane_x = camera.plane.x * Math.cos(-rot_speed) - camera.plane.y * Math.sin(-rot_speed);
                const new_plane_y = camera.plane.x * Math.sin(-rot_speed) + camera.plane.y * Math.cos(-rot_speed);

                const change_transform = this.emit_component_message(ChangeTransformMessage, transform);
                change_transform.new_direction = new Vector(new_dir_x, new_dir_y);
                camera.plane.x = new_plane_x;
                camera.plane.y = new_plane_y;

            }
        }

    }
}