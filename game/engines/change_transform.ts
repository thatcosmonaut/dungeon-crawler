import { Engine, Reads, Message, Type } from "encompass-ecs";
import { ChangeTransformMessage } from "game/messages/change_transform";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(ChangeTransformMessage)
export class ChangeTransformEngine extends Engine {

    public update() {
        const change_transform_messages = this.read_messages(ChangeTransformMessage);
        for (const change_transform_message of change_transform_messages.iterable()) {
            if (change_transform_message.new_direction != null) {
                change_transform_message.component.direction = change_transform_message.new_direction;
            }

            if (change_transform_message.new_position != null) {
                change_transform_message.component.position = change_transform_message.new_position;
            }

        }
    }
}