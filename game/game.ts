import { World, WorldBuilder } from "encompass-ecs";
import { ChangeTransformEngine } from "./engines/change_transform";
import { CreateTileEngine } from "./engines/create_tile";
import { DestroyLevelDetecter } from "./engines/destroyer";
import { GenerateLevelEngine } from "./engines/generate_level";
import { InputEngine } from "./engines/input";
import { SpawnPlayerEngine } from "./engines/spawn_player";
import { GenerateLevelMessage } from "./messages/generate_level";
import { FirstPersonRenderer } from "./renderers/firstperson";

export class Game {
    private world: World;
    private canvas: Canvas;

    public load() {
        this.canvas = love.graphics.newCanvas();

        const world_builder = new WorldBuilder();

        // ADD YOUR ENGINES HERE...
        world_builder.add_engine(GenerateLevelEngine);
        world_builder.add_engine(CreateTileEngine);
        world_builder.add_engine(DestroyLevelDetecter);
        world_builder.add_engine(InputEngine);
        world_builder.add_engine(SpawnPlayerEngine);
        world_builder.add_engine(ChangeTransformEngine);

        // ADD YOUR RENDERERS HERE...
        world_builder.add_renderer(FirstPersonRenderer).initialize();

        const generate_level = world_builder.emit_message(GenerateLevelMessage);
        generate_level.size = 20;

        this.world = world_builder.build();
    }

    public update(dt: number) {
        this.world.update(dt);
    }

    public draw() {
        love.graphics.clear();
        love.graphics.setCanvas(this.canvas);
        love.graphics.clear();
        this.world.draw();
        love.graphics.setCanvas();
        love.graphics.setBlendMode("alpha", "premultiplied");
        love.graphics.setColor(1, 1, 1, 1);
        love.graphics.draw(this.canvas);
    }
}
