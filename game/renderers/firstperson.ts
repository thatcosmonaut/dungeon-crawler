import { GeneralRenderer } from "encompass-ecs";
import { CameraComponent } from "game/components/camera";
import { LevelComponent } from "game/components/level";
import { TileComponent } from "game/components/tile";
import { TransformComponent } from "game/components/transform";
import { GCOptimizedMap, GCOptimizedSet } from "tstl-gc-optimized-collections";

export class FirstPersonRenderer extends GeneralRenderer {
    public layer = 0;

    private tile_map = new GCOptimizedMap<number, TileComponent>();
    private buffer_canvas: Canvas;
    private width_scale: number = 1;
    private height_scale: number = 1;
    private width: number = 320;
    private height: number = 240;

    public initialize() {
        this.buffer_canvas = love.graphics.newCanvas(this.width, this.height);
    }

    public render() {
        const canvas = love.graphics.getCanvas()!;

        const levels = this.read_components(LevelComponent);

        for (const level of levels.iterable()) {

            const tiles = this.read_components(TileComponent);
            const cameras = this.read_components(CameraComponent);

            for (const tile of tiles.iterable()) {
                const entity = this.get_entity(tile.entity_id);
                if (entity !== undefined) {
                    const transform = entity.get_component(TransformComponent);
                    if (transform !== undefined) {
                        const existing = this.tile_map.get(level.size * transform.position.y + transform.position.x);
                        if (existing !== tile) {
                            this.tile_map.set(level.size * transform.position.y + transform.position.x, tile);
                        }

                    }
                }
            }

            for (const camera of cameras.iterable()) {
                const camera_entity = this.get_entity(camera.entity_id);
                if (camera_entity != null) {
                    const w = this.buffer_canvas.getWidth();
                    const h = this.buffer_canvas.getHeight();
                    const transform = camera_entity.get_component(TransformComponent);

                    love.graphics.setCanvas(this.buffer_canvas);
                    love.graphics.clear();
                    for (let x = 0; x < w; x++) {
                        const camera_x = 2 * x / w - 1;
                        const ray_dir_x = transform.direction.x + camera.plane.x * camera_x;
                        const ray_dir_y = transform.direction.y + camera.plane.y * camera_x;

                        let map_x = Math.floor(transform.position.x);
                        let map_y = Math.floor(transform.position.y);

                        let side_dist_x = 0;
                        let side_dist_y = 0;

                        const delta_dist_x = Math.abs(1 / ray_dir_x);
                        const delta_dist_y = Math.abs(1 / ray_dir_y);
                        let perp_wall_dist;

                        let step_x = 0;
                        let step_y = 0;

                        let hit = false;
                        let side = 0;

                        if (ray_dir_x < 0) {
                            step_x = -1;
                            side_dist_x = (transform.position.x - map_x) * delta_dist_x;
                        } else {
                            step_x = 1;
                            side_dist_x = (map_x + 1.0 - transform.position.x) * delta_dist_x;
                        }

                        if (ray_dir_y < 0) {
                            step_y = -1;
                            side_dist_y = (transform.position.y - map_y) * delta_dist_y;
                        } else {
                            step_y = 1;
                            side_dist_y = (map_y + 1.0 - transform.position.y) * delta_dist_y;
                        }

                        while (!hit) {
                            if (side_dist_x < side_dist_y) {
                                side_dist_x += delta_dist_x;
                                map_x += step_x;
                                side = 0;
                            } else {
                                side_dist_y += delta_dist_y;
                                map_y += step_y;
                                side = 1;
                            }
                            const t = this.tile_map.get(level.size * map_y + map_x);
                            if (t != null && !t.walkable) {
                                hit = true;
                            }
                        }

                        if (side === 0) {
                            perp_wall_dist = (map_x - transform.position.x + (1 - step_x) / 2) / ray_dir_x;
                        } else {
                            perp_wall_dist = (map_y - transform.position.y + (1 - step_y) / 2) / ray_dir_y;
                        }

                        const line_height = Math.floor(h / perp_wall_dist);
                        let draw_start = -line_height / 2 + h / 2;
                        if (draw_start < 0) { draw_start = 0; }
                        let draw_end = line_height / 2 + h / 2;
                        if (draw_end >= h) { draw_end = h - 1; }

                        const tile = this.tile_map.get(level.size * map_y + map_x);

                        if (tile != null) {
                            let wall_x;
                            if (side === 0) { wall_x = transform.position.y + perp_wall_dist * ray_dir_y; } else { wall_x = transform.position.x + perp_wall_dist * ray_dir_x; }
                            wall_x -= Math.floor(wall_x);

                            let tex_x = Math.floor(wall_x * tile.texture.getWidth());
                            if (side === 0 && ray_dir_x > 0) { tex_x = tile.texture.getWidth() - tex_x - 1; }
                            if (side === 1 && ray_dir_y < 0) { tex_x = tile.texture.getWidth() - tex_x - 1; }

                            for (let y = draw_start; y < draw_end; y++) {
                                const d = y * 256 - h * 128 + line_height * 128;
                                const tex_y = Math.floor(((d * tile.texture.getHeight()) / line_height) / 256);
                                const color = tile.texture.getPixel(tex_x, tex_y);
                                love.graphics.setColor(color);
                                love.graphics.points(x, y);
                            }
                        }
                    }
                    love.graphics.setCanvas(canvas);
                }
            }
        }

        if (canvas !== null) {
            const pixel_width = Math.floor(this.width_scale * canvas.getWidth());
            const pixel_height = Math.floor(this.height_scale * canvas.getHeight());
            love.graphics.setColor(1, 1, 1, 1);
            love.graphics.setBlendMode("alpha", "premultiplied");
            love.graphics.draw(this.buffer_canvas, 0, 0, 0, pixel_width / this.width, pixel_height / this.height);
        }
    }
}
