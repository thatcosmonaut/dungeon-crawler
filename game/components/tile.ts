import { Component } from "encompass-ecs";

export class TileComponent extends Component {
    public walkable: boolean;
    public texture: ImageData;
}